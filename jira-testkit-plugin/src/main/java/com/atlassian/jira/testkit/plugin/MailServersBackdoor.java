/*
 * Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under the License.
 */

package com.atlassian.jira.testkit.plugin;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.ComponentClassManager;
import com.atlassian.jira.service.JiraServiceContainer;
import com.atlassian.jira.service.ServiceManager;
import com.atlassian.jira.service.services.file.AbstractMessageHandlingService;
import com.atlassian.mail.MailException;
import com.atlassian.mail.MailFactory;
import com.atlassian.mail.MailProtocol;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.mail.server.MailServer;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.mail.server.PopMailServer;
import com.atlassian.mail.server.SMTPMailServer;
import com.atlassian.mail.server.impl.PopMailServerImpl;
import com.atlassian.mail.server.impl.SMTPMailServerImpl;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Use this backdoor to manipulate Mail Servers as part of setup for tests.
 *
 * This class should only be called by the <code>com.atlassian.jira.testkit.client.MailServersControl</code>.
 *
 * @since v5.0
 */
@Path ("mailServers")
@AnonymousAllowed
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
public class MailServersBackdoor
{
    private static final Logger LOGGER = LoggerFactory.getLogger(MailServersBackdoor.class);
    private final MailQueue mailQueue;
    private final ServiceManager serviceManager;
    private final ComponentClassManager componentClassManager;


    public MailServersBackdoor(final MailQueue mailQueue, final ServiceManager serviceManager)
    {
        this.mailQueue = mailQueue;
        this.serviceManager = serviceManager;
        this.componentClassManager = ComponentAccessor.getComponentClassManager();
    }

    @POST
    @Path("smtp")
    public Response addSmtpServer(MailServersBean mailServerBean) throws MailException
    {
        // 1. Premptive strike against failing tests - fall over if the SMTP server won't be able to send anything.
        if (MailFactory.isSendingDisabled())
        {
            throw new IllegalStateException("Mail sending is disabled. Please restart your server without"
                    + " -Datlassian.mail.senddisabled=true.");
        }

        // 2. Delete any existing SMTP servers
        MailServerManager mailServerManager = MailFactory.getServerManager();
        List<SMTPMailServer> smtpMailServers = mailServerManager.getSmtpMailServers();
        for (SMTPMailServer smtpMailServer : smtpMailServers)
        {
            mailServerManager.delete(smtpMailServer.getId());
        }

        // 3. Add the new server
        MailServer mailServer = new SMTPMailServerImpl(null, mailServerBean.name, mailServerBean.description,
                mailServerBean.from, mailServerBean.prefix, false, mailServerBean.secure == true ? MailProtocol.SECURE_SMTP : MailProtocol.SMTP, mailServerBean.serverName,
                mailServerBean.port, mailServerBean.tls == null ? false : mailServerBean.tls,
                mailServerBean.username, mailServerBean.password, 10000L);
        mailServerManager.create(mailServer);

        return Response.ok(null).build();
    }

    @POST
    @Path("pop")
    public Response addPopServer(MailServersBean mailServerBean) throws MailException
    {
        PopMailServer mailServer = new PopMailServerImpl(null, mailServerBean.name, mailServerBean.description,
                mailServerBean.serverName, mailServerBean.username, mailServerBean.password);
        if (StringUtils.isNotBlank(mailServerBean.port))
        {
            mailServer.setPort(mailServerBean.port);
        }
        if (StringUtils.isNotBlank(mailServerBean.protocol))
        {
            mailServer.setMailProtocol(MailProtocol.getMailProtocol(mailServerBean.protocol));
        }

        MailServerManager mailServerManager = MailFactory.getServerManager();
        mailServerManager.create(mailServer);

        return Response.ok(null).build();
    }

    @DELETE
    @Path("incoming")
    public Response deleteIncomingServersAndHandlers() throws Exception
    {
        MailServerManager mailServerManager = MailFactory.getServerManager();
        for (PopMailServer popMailServer : mailServerManager.getPopMailServers())
        {
            mailServerManager.delete(popMailServer.getId());
        }

        final ImmutableList<JiraServiceContainer> mailHandlerServices = ImmutableList.copyOf(Iterables.filter(serviceManager.getServices(), new IsMailHandlerFilter()));
        for (JiraServiceContainer jiraServiceContainer : mailHandlerServices)
        {
            serviceManager.removeService(jiraServiceContainer.getId());
        }

        return Response.ok().build();
    }

    @DELETE
    @Path("outgoing")
    public Response deleteSmtpServersAndHandlers() throws Exception
    {
        MailServerManager mailServerManager = MailFactory.getServerManager();
        for (SMTPMailServer smtpMailServer : mailServerManager.getSmtpMailServers())
        {
            mailServerManager.delete(smtpMailServer.getId());
        }
        return Response.ok().build();
    }

    @GET
    @Path("flush")
    public Response flushMailQueue()
    {
        LOGGER.info("Flushing mail Queue - currentQueueSize = {}", mailQueue.size());
        LOGGER.debug("Mail queue of type: {}, from classloader: {}, ",
                mailQueue.toString(),
                mailQueue.getClass().getClassLoader().toString());

        mailQueue.sendBuffer();
        return Response.ok().build();
    }

    @GET
    @Path ("smtpConfigured")
    public Response smtpMailConfigured()
    {
        boolean configured = true;
        if (MailFactory.isSendingDisabled())
        {
            configured = false;
        }
        MailServerManager mailServerManager = MailFactory.getServerManager();
        if (!mailServerManager.isDefaultSMTPMailServerDefined())
        {
            configured = false;
        }

        return Response.ok(configured).build();
    }

    final class IsMailHandlerFilter implements Predicate<JiraServiceContainer>
    {
        public boolean apply(JiraServiceContainer jiraServiceContainer) {
            try {
                return AbstractMessageHandlingService.class.isAssignableFrom(componentClassManager.loadClass(jiraServiceContainer.getServiceClass()));
            }
            catch (ClassNotFoundException e)
            {
                return false;
            }
        }
    }
}
